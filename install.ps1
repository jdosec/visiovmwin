# { iwr -useb https://gitlab.com/jdosec/visiovmwin/-/raw/master/install.ps1 } | iex; install
# { iwr -useb https://annuel.framapad.org/p/install.ps1/export/txt } | iex; install
# iex (irm https://gitlab.com/jdosec/visiovmwin/-/raw/master/install.ps1)
# iex (irm https://annuel.framapad.org/p/install.ps1/export/txt)

Set-ExecutionPolicy Bypass -Scope Process -Force

# install chocolatey if not installed
if (!(Test-Path -Path "$env:ProgramData\Chocolatey")) {
  Invoke-Expression((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}

# for each package in the list run install
# if you want to use an etherpad, here is an example
# $urlPackages = "https://annuel.framapad.org/p/chocolateyPackages/export/txt"
$urlPackages = "https://gitlab.com/jdosec/visiovmwin/-/raw/master/packages.txt"
$output = "packages.txt"
#$start_time = Get-Date
#(New-Object System.Net.WebClient).DownloadFile($url, $output) 
wget -Uri $urlPackages -UseBasicParsing -OutFile $output
Get-Content $output | ForEach-Object{($_ -split "\r\n")[0]} | ForEach-Object{choco install -y $_}

choco install --force choco-upgrade-all-at --params "'/DAILY:yes /TIME:04:00 /ABORTTIME:08:00'"
