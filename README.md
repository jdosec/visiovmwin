# Copyright Jérôme Dossogne all rights reserved

# Authors
* Jérôme Dossogne

# Contributors
/

# Visibility
Project is currently public.

# Tested on
https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/

# Usage
1. Install your windows 10 VM
2. Open an Adminstrator Powershell Shell (Requires -RunAsAdministrator)
3. Type `iex (irm https://gitlab.com/jdosec/visiovmwin/-/raw/master/install.ps1)`
	* This is what is typically being called a "download cradle". There are various types. I use that one because ... it works with the vm I used to test. See also:
		* https://mgreen27.github.io/posts/2018/04/02/DownloadCradle.html
		* https://gist.github.com/HarmJ0y/bb48307ffa663256e239
4. Enjoy your coffee break
	* installing everything takes a long while
	* the script installs visio/conference/communication software firsts and then other things useful to open files etc.

# To-do list / Future work
* change keyboard layout
* set resolution to 1920*1080 for streaming (host in 4k can zoom to 175% and everyone is happy)
* install windows updates
* hardenning
	* start by changing password

# Multi-VMs setup suggestions
* Use one to stream/visio and remote display (VRDP) to the others
	* https://www.virtualbox.org/manual/ch07.html
	* mstsc 1.2.3.4:3389
		* where "1.2.3.4" is the HOST IP, not the VM ip
	* rdesktop -a 16 -N 1.2.3.4:3389
* On your Windows machine, activate Remote Desktop
* On your linux machine
	* If you have Ubuntu Mate
		* sudo apt install xrdp && sudo systemctl enable xrdp && sudo adduser xrdp ssl-cert && echo mate-session> ~/.xsession #https://askubuntu.com/questions/678979/remote-desktop-rdp-blank-screen-and-aborts-after-disconnect
		* Consider also the vbox additions
			* sudo ./VBoxLinuxAdditions.run && adduser <username> vboxsf
			* xrandr --output Virtual1 --mode "1920x1080" --scale "0.5x0.5"
* Put the 3 in the same NAT network
	* VBoxManage natnetwork add --netname natnet1 --network "192.168.15.0/24" --enable
	* https://www.virtualbox.org/manual/ch06.html#network_nat
