signal
telegram
jitsi
steam
teamviewer
pidgin
skype
rocketchat
slack
jitsi-meet-electron
mattermost-desktop
zoom
gotomeeting
microsoft-teams.install
microsoft-teams
webex-teams
discord
WhatsApp
mumble
nextcloud-client
googlechrome
googledrive
chromium
dropbox
epicgameslauncher
filezilla
firefox
firefoxesr
openssh
openvpn
notepadplusplus
virtualbox-guest-additions-guest.install
chocolatey
chocolatey-core.extension
chocolateygui
choco-upgrade-all-at
curl
cygwin
evernote
exiftool
flashplayeractivex
flashplayerplugin
foxitreader
gpg4win
graphviz
greenshot
hashtab
hxd
irfanview
irfanviewplugins
javaruntime
kdiff3
keepass
libreoffice
markdownmonster
mbca
meld
mkdocs
mpc-be
mpc-hc
nano
opera
processhacker
procexp
procmon
putty
python
ruby
sdelete
silverlight
smartmontools
speccy
spotify
sublimetext2
sublimetext3
sumatrapdf
sysinternals
teracopy
thebrain
tor-browser
totalcommander
treesizefree
veracrypt
vlc
wget
which
windirstat
winmerge
winrar
winscp
xmind
xnview
yed
youtube-dl
